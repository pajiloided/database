# Настройки

<glossary-variable color="red">

## windowBackgroundGrayShadow

Задает цвет тени между секциями, например, в Настройках.

</glossary-variable>

<figure>

![](./images/settings.0.png)

<figcaption>

Красное — `windowBackgroundGrayShadow`.

</figcaption>
</figure>

## Иконки

<glossary-variable color="red">

### changephoneinfo_image

Задает цвет иконок симкарт и стрелки на экране в Настройка → Сменить номер.

</glossary-variable>

<glossary-variable color="green">

### sessions_devicesImage

Задает цвет картинки телефона или планшета, когда вы вошли с одного устройства,
на экране в Настройках → Устройства.

</glossary-variable>

<figure>

![](./images/settings.1.png)

<figcaption>

Красное — `chagephoneinfo_image`, зеленое — `sessions_devicesImage`.

</figcaption>
</figure>

## Стикеры и темы

<glossary-variable color="blue">

### stickers_menu

Задает цвет иконки троеточия рядом со стикером или темой.

</glossary-variable>

<glossary-variable color="red">

### stickers_menuSelector

Задает цвет оверлея при нажатии на иконку.

</glossary-variable>

<figure>

![](./images/settings.2.png)

<figcaption>

Синее — `stickers_menu`, красное — `stickers_menuSelector`.

</figcaption>
</figure>

<glossary-variable color="orange">

### featuredStickers_addedIcon

Задает цвет галочки рядом с добавленным паком стикеров или выбранной темой.

</glossary-variable>

<glossary-variable color="red">

### featuredStickers_addButton

Задает цвет фона кнопки "Добавить".

</glossary-variable>

<glossary-variable color="blue">

### featuredStickers_addButtonSelected

Задает цвет фона кнопки "Добавить" при нажатии.

</glossary-variable>

<glossary-variable color="green">

### featuredStickers_buttonText

Задает цвет текста "Добавить" или "Удалить" на кнопке.

</glossary-variable>

<glossary-variable color="purple">

### featuredStickers_buttonProgress

Задает цвет кружка прогресса, при добавлении или удалении пака стикеров.

</glossary-variable>

<figure>

![](./images/settings.3.png)

<figcaption>

Красное — `featuredStickers_addButton`, синее —
`featuredStickers_addButtonSelected`, зеленое — `featuredStickers_buttonText`,
оранжевое — `featuredStickers_addedIcon`, фиолетовое —
`featuredStickers_buttonProgress`.

</figcaption>
</figure>
