# Contacts

The next two variables are used not only in Contacts, but primarily they are
seen here.

<glossary-variable color="green">

## windowBackgroundWhiteIcon

Sets the color of icons.

</glossary-variable>

<glossary-variable color="red">

## graySection

Sets the header background color like “Contacts”. The title on this header is
set by `windowBackgroundWhiteGrayText2`.

</glossary-variable>

<figure>

![](./images/contacts.0.png)

<figcaption>

Red — `graySection`, green — `windowBackgroundWhiteIcon`.

</figcaption>
</figure>

## Fast scroll

<glossary-variable color="green">

### fastScrollInactive

Sets the color of the scrollbar when not pressed.

</glossary-variable>

<glossary-variable color="red">

### fastScrollActive

Sets the color of the scrollbar when you hold and move it and the bubble that
contains the letter on where you are.

</glossary-variable>

<glossary-variable color="orange">

### fastScrollText

Sets the color of the letter inside the bubble when you hold and move the
scrollbar.

</glossary-variable>

<figure>

![](./images/contacts.1.png)

<figcaption>

Green — `fastScrollInactive`, red — `fastScrollActive`, orange —
`fastScrollText`.

</figcaption>
</figure>

## “Invite friends” screen

<glossary-variable color="red">

### contacts_inviteBackground

Sets the background of the bottom hint on the “Invite friends” screen.

</glossary-variable>

<glossary-variable color="yellow">

### contacts_inviteText

Sets the text color of the hint.

</glossary-variable>

<figure>

![](./images/contacts.2.png)

<figcaption>

Red — `contacts_inviteBackground`, yellow — `contacts_inviteText`.

</figcaption>
</figure>
