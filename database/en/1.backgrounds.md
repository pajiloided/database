# Backgrounds

<glossary-variable color="red">

## windowBackgroundWhite

Sets the background almost everywhere except the chat screen background. For
example, it sets the background on the chats list screen or the background of
sections in Settings.

</glossary-variable>

<glossary-variable color="green">

## windowBackgroundGray

Sets the secondary background, e. g. in Settings, between sections or below
calls log if it doesn't fill the screen fully.

</glossary-variable>

<figure>

![](./images/backgrounds.0.png)

<figcaption>

The red areas show `windowBackgroundWhite` and the green areas show
`windowBackgroundGray`.

</figcaption>
</figure>

<glossary-variable color="gray">

## chat_wallpaper

Sets the background on the chat screen, accepts either an image or a color. You
must change _this variable_ so Telegram includes your wallpaper in your theme.
Changing the wallpaper in Settings → Chat Wallpaper won't include it in your
theme.

**Be careful:** if you set an image as a wallpaper, Telegram will compress it
every time you change any variable with the in-app editor. For this reason, you
must set your image again before you share your theme.

</glossary-variable>

<figure>

![](./images/backgrounds.1.png)

<figcaption>

Example of an image and a color as a chat screen wallpaper.

</figcaption>
</figure>
