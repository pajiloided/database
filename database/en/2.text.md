# Text

<glossary-variable color="red">

## windowBackgroundWhiteBlackText

Sets the color of almost all texts in the app.

**Be careful:** the bot inline result titles color is also set by this variable,
although the background of the results isn’t set by windowBackgroundWhite but
chat_messagePanelBackground. Consider setting the message panel background the
same as windowBackgroundWhite or check the readability of the bot results.

</glossary-variable>

<figure>

![](./images/text.0.png)

<figcaption>

The red areas show `windowBackgroundWhiteBlackText`.

</figcaption>
</figure>

## Gray text

<glossary-variable color="red">

### windowBackgroundWhiteGrayText

Sets the last seen time color in Contacts.

</glossary-variable>

<glossary-variable color="purple">

### windowBackgroundWhiteGrayText2

Sets the color of descriptions below option titles in Settings. Also sets the
color of the title on `graySection`.

</glossary-variable>

<glossary-variable color="blue">

### windowBackgroundWhiteGrayText3

Sets the color of a session’s IP in Settings → Privacy and Security → Active
Sessions.

</glossary-variable>

<glossary-variable color="lightBlue">

### windowBackgroundWhiteGrayText4

Sets the color of description texts below sections in Settings.

</glossary-variable>

<glossary-variable color="green">

### windowBackgroundWhiteGrayText5

Sets the color of Telegram version at the bottom in Settings.

</glossary-variable>

<glossary-variable color="yellow">

### windowBackgroundWhiteGrayText6

Sets the color of description texts below text fields like in Settings → Phone →
Change Number and the color of the info text in “What is a Channel?” screen that
appears when you try to create a channel (unless you already have one).

**Be careful:** this variable sets the color of text like “Please enter your
password” on the gray background in Settings → Privacy and Security → Two-step
Verification/Passcode Lock when you have either a two-step or a passcode lock
password when you have (one of) them set.

</glossary-variable>

<glossary-variable color="pink">

### windowBackgroundWhiteGrayText7

Sets the color of a disabled item in Settings → Privacy and Security → Passcode
Lock when you have no passcode lock.

</glossary-variable>

<glossary-variable color="brown">

### windowBackgroundWhiteGrayText8

Sets the color of the info text in Settings → Username.

</glossary-variable>

<figure>

![](./images/text.1.png)

<figcaption>

The red area at the top shows `windowBackgroundWhiteGrayText`, the purple area
shows `windowBackgroundWhiteGrayText2`, the dark blue area shows
`windowBackgroundWhiteGrayText3`, the light blue area shows
`windowBackgroundWhiteGrayText4`, the green area shows
`windowBackgroundWhiteGrayText5`, the yellow area shows
`windowBackgroundWhiteGrayText6`, the pink area at the bottom shows
`windowBackgroundWhiteGrayText7`, and the brown area shows
`windowBackgroundWhiteGrayText8`.

</figcaption>
</figure>

## Red text

<glossary-variable color="red">

### windowBackgroundWhiteRedText

Sets the color of red buttons on dialogs like the “Clear Cache” button in
Settings → Data and Storage → Storage Usage → Clear Cache.

You can’t change this variable with the in-app editor for unknown reasons, so
you have to use [.attheme editor](http://snejugal.ru/attheme-editor) to change
this variable.

</glossary-variable>

<glossary-variable color="blue">

### windowBackgroundWhiteRedText2

Sets the color of red buttons in Settings like the “Terminate All Other
Sessions” button in Settings → Privacy and Security → Active Sessions.

</glossary-variable>

<glossary-variable color="green">

### windowBackgroundWhiteRedText3

Sets the color of the “Abort two-step verification setup” button in Settings →
Privacy and Security → Two-step Verification while setting up a two-step
password.

</glossary-variable>

<glossary-variable color="purple">

### windowBackgroundWhiteRedText4

Sets the color of warning text below a text field like in Settings → Username
when you try to take a username less than 5 symbols or already taken.

</glossary-variable>

<glossary-variable color="lightBlue">

### windowBackgroundWhiteRedText5

Sets the color of warning buttons like “Leave Channel” in the channel info
screen.

</glossary-variable>

<glossary-variable color="yellow">

### windowBackgroundWhiteRedText6

Sets the color of “Reset my account” button when you try to sign in your account
that has a two-step verification password but you don’t remember it, try to
restore it with your email but you can’t access it.

This variable can’t be changed with the in-app editor (because it closes after
you sign out), so you have to use
[.attheme editor](http://snejugal.ru/attheme-editor) to change this variable.

</glossary-variable>

<figure>

![](./images/text.2.png)

<figcaption>

The red area shows `windowBackgroundWhiteRedText`, the dark blue area shows
`windowBackgroundWhiteRedText2`, the green area shows
`windowBackgroundWhiteRedText3`, the purple area shows
`windowBackgroundWhiteRedText4`, the light blue area shows
`windowBackgroundWhiteRedText5`, and the yellow area shows
`windowBackgroundWhiteRedText6`.

</figcaption>
</figure>

## Green text

<glossary-variable color="red">

### windowBackgroundWhiteGreenText

Sets the color of success text below text fields like in Settings → Username
when you take a username no one has and longer than 5 symbols.

</glossary-variable>

<glossary-variable color="blue">

### windowBackgroundWhiteGreenText2

Sets the color of green buttons like “Start Secret Chat” in the chat with a
person info screen.

</glossary-variable>

<figure>

![](./images/text.3.png)

<figcaption>

The red area shows `windowBackgroundWhiteGreenText` and the blue area shows
`windowBackgroundWhiteGreenText2`.

</figcaption>
</figure>

## Blue text

<glossary-variable color="red">

### windowBackgroundWhiteBlueHeader

Sets the color of section header in Settings.

</glossary-variable>

<glossary-variable color="purple">

### windowBackgroundWhiteBlueText

Sets the color of the Online status text in Contacts.

</glossary-variable>

<glossary-variable color="blue">

### windowBackgroundWhiteBlueText3

Sets the color of the Online status text in search results.

</glossary-variable>

<glossary-variable color="lightBlue">

### windowBackgroundWhiteBlueText4

Sets the color of the “Change Number” button in Settings → Phone and also sets
the match highlights in search results.

</glossary-variable>

<glossary-variable color="green">

### windowBackgroundWhiteBlueText5

Sets the color of “Create Channel” button in “What is a Channel?” screen that
appears when you try to create a new channel (unless you already have one). If
the screen doesn’t appear to you, then use
[.attheme editor](http://snejugal.ru/attheme-editor) to change this variable.

</glossary-variable>

<glossary-variable color="orange">

### windowBackgroundWhiteBlueText6

Sets the color of the pay button after you entered all payment data when you’re
buying something. You can make a fake pay with [@ShopBot](https://t.me/shopbot)
to see the button or use [.attheme editor](http://snejugal.ru/attheme-editor) to
add change the variable.

</glossary-variable>

<glossary-variable color="teal">

### windowBackgroundWhiteBlueText7

Sets the “Send your current location” button color in location attachment
screen.

</glossary-variable>

<glossary-variable color="lightGreen">

### windowBackgroundWhiteValueText

Sets the option value color in Settings.

</glossary-variable>

<glossary-variable color="deepOrange">

### windowBackgroundWhiteLinkText

Sets the color of links, like #hashtags, @usernames or just links in the chat
info screen.

</glossary-variable>

<figure>

![](./images/text.4.png)

<figcaption>

The red area shows `windowBackgroundWhiteBlueHeader`, the purple area shows
`windowBackgroundWhiteBlueText`, the dark blue area shows
`windowBackgroundWhiteBlueText3`, the light blue areas show
`windowBackgroundWhiteBlueText4`, the green area shows
`windowBackgroundWhiteBlueText5`, the light orange area is
`windowBackgroundWhiteBlueText6`, the teal area shows
`windowBackgroundWhiteBlueText7`, the light green one shows
`windowBackgroundWhiteValueText`, and the dark orange area shows
`windowBackgroundWhiteLinkText`.

</figcaption>
</figure>

<glossary-variable color="red">

### windowBackgroundWhiteLinkSelection

Sets the overlay color that appears when you tap a link.

</glossary-variable>

<figure>

![](./images/text.5.png)

<figcaption>

The red area shows `windowBackgroundWhiteLinkSelection`.

</figcaption>
</figure>
